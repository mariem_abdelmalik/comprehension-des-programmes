package visialisation;

import java.io.IOException;
import java.util.Set;

import grapheCouplage.*;
import parsage.Parser;

public class visialisation {

	public static void main(String[] args) throws IOException {

		final String projectPath = "/home/mariem/Bureau/test";

		Parser parser = new Parser(projectPath);

		GrapheCouplageWeighted grapheCouplageWeighted = new GrapheCouplageWeighted();

		grapheCouplageWeighted.initGrapheCouplageWeighted(parser.getModel());

		new UndirectedWeightedGraph(grapheCouplageWeighted.getEdgs());

	}
}
