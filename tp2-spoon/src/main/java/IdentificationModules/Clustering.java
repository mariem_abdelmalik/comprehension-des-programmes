package IdentificationModules;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.TypeDeclaration;

import spoon.reflect.CtModel;
import spoon.reflect.declaration.CtType;

public class Clustering {

	public Set<String> classes = new HashSet<String>();

	private CtModel model;

	public Cluster Clustring_hierarchique() {

		Set<Cluster> clusters = new HashSet<Cluster>();

		for (String C : classes) {

			Cluster cluster = new Cluster();

			cluster.addClasses(C);

			clusters.add(cluster);
		}

		ArrayList<Cluster> clusterProche = new ArrayList<Cluster>();

		Cluster c1 = new Cluster();

		Cluster c2 = new Cluster();

		Cluster c3 = new Cluster();

		while (clusters.size() > 1) {

			clusterProche = this.clusterProche(clusters);

			c1 = clusterProche.get(0);

			c2 = clusterProche.get(1);

			c3 = mergeClusters(c1, c2);

			clusters.remove(c1);

			clusters.remove(c2);

			clusters.add(c3);

		}

		Iterator iter = clusters.iterator();

		Cluster dendo = (Cluster) iter.next();

		return dendo;

	}

	// a implementer 
	public ArrayList<Cluster> clusterProche(Set<Cluster> clusters) {

		return null;
		
	}

	public Cluster mergeClusters(Cluster c1, Cluster c2) {

		Cluster result = new Cluster();

		result.getClasses().addAll(c1.getClasses());

		result.getClasses().addAll(c2.getClasses());

		return result;

	}

	public void initClasses() {

		
		for (CtType<?> c : model.getAllTypes()) {

			System.out.println(c.toString());
			
			classes.add(c.toString());

		}

	}
	
}
