package IdentificationModules;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.core.dom.TypeDeclaration;

public class Cluster {

	public Set<String> classes = new HashSet<String>();
	
	public void addClasses(String c) {
		
		this.classes.add(c);
	}

	public Set<String> getClasses() {
		return classes;
	}

	public void setClasses(Set<String> classes) {
		this.classes = classes;
	}
	
	
}
