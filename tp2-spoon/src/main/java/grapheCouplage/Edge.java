package grapheCouplage;

public class Edge { 
	
	Node nodeA, nodeB;
	Double Weighte;
	
	public Edge(Node nodeA, Node nodeB,Double Weighte) { 
		this.nodeA = nodeA; this.nodeB = nodeB; this.Weighte=Weighte;
		}

	public Node getNodeA() {
		return nodeA;
	}

	public void setNodeA(Node nodeA) {
		this.nodeA = nodeA;
	}

	public Node getNodeB() {
		return nodeB;
	}

	public void setNodeB(Node nodeB) {
		this.nodeB = nodeB;
	}
	
	@Override
	public boolean equals (Object object) {
		
		if (this.nodeA.name==((Edge)object).nodeA.name && this.nodeB.name==((Edge)object).nodeB.name) {
			return true;
		}
		return false;
		
	}

	public Double getWeighte() {
		return Weighte;
	}

	public void setWeighte(Double weighte) {
		Weighte = weighte;
	}
	
	
	
	
} 
