package grapheCouplage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import spoon.reflect.CtModel;
import spoon.reflect.code.CtInvocation;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtType;
import spoon.reflect.visitor.CtVisitor;
import spoon.reflect.visitor.Query;
import spoon.reflect.visitor.filter.TypeFilter;

public class Couplage {

	private CtModel model;

	private int totalAppel;

	public int nbAppelMethod(CtType A, CtType B) {

		int appelAB = 0;

		Set<CtMethod> methodsA = A.getMethods();
		Set<CtMethod> methodsB = B.getMethods();

		A.getMethods();

		for (CtMethod methodA : methodsA) {

			for (CtInvocation<?> methodInvocation : Query.getElements(methodA,
					new TypeFilter<CtInvocation<?>>(CtInvocation.class))) {

				for (CtMethod methodB : methodsA) {

					if (methodB.toString().equals(methodInvocation.toString())) {
						appelAB = appelAB + 1;
					}
				}

			}
		}

		return appelAB;

	}

	public void initTotalAppel() {

		ArrayList<CtType> classes = new ArrayList<CtType>(this.model.getAllTypes());

		for (int i = 0; i < classes.size() - 1; i++) {

			for (int j = i + 1; j < classes.size(); j++) {

				this.totalAppel = this.totalAppel + this.nbAppelMethod(classes.get(i), classes.get(j));

				this.totalAppel = this.totalAppel + this.nbAppelMethod(classes.get(j), classes.get(i));

			}
		}
	}

	

	public Double couplage(CtType A, CtType B) {

		Double couplageAB = (double) 0;

		Double nbAppelAB = (double) this.nbAppelMethod(A, B);

		Double nbAppelBA = (double) this.nbAppelMethod(B, A);

		couplageAB = (nbAppelAB + nbAppelBA) / this.totalAppel;

		return couplageAB;

	}

	public int getTotalAppel() {
		return totalAppel;
	}

	public void setTotalAppel(int totalAppel) {
		this.totalAppel = totalAppel;
	}

}
