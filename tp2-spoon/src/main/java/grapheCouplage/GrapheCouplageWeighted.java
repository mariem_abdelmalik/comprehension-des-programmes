package grapheCouplage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.TypeDeclaration;

import spoon.reflect.CtModel;
import spoon.reflect.declaration.CtType;

public class GrapheCouplageWeighted {

	Set<Node> nodes = new HashSet<Node>();
	Set<Edge> edgs = new HashSet<Edge>();

	public void initGrapheCouplageWeighted(CtModel model) {

		Couplage couplage = new Couplage();
		ArrayList<CtType> classes = new ArrayList<CtType>(model.getAllTypes());

		for (int i = 0; i < classes.size() - 1; i++) {

			for (int j = i + 1; j < classes.size(); j++) {

				if (couplage.couplage(classes.get(i), classes.get(j)) > 0) {

					Node nodeA = new Node(classes.get(i).getSimpleName().toString());
					Node nodeB = new Node(classes.get(j).getSimpleName().toString());

					nodes.add(nodeA);
					nodes.add(nodeB);

					Edge edge = new Edge(nodeA, nodeB, couplage.couplage(classes.get(i), classes.get(j)));

					edgs.add(edge);

				}

			}
		}

	}

	// verification de l'existance d'edge dans la liste des edges.
	public int edgeAlreadyExists(Edge edge, List<Edge> edges) {

		for (int i = 0; i < edges.size(); i++) {

			if (edge.getNodeA().getName().equals(edges.get(i).getNodeA().getName())
					&& edge.getNodeB().equals(edges.get(i).getNodeB().getName())) {

				return i;

			}
		}

		return -1;
	}

	public ArrayList<Node> getNodes() {
		return (ArrayList<Node>) nodes;
	}

	public Set<Edge> getEdgs() {
		return edgs;
	}

	public void setEdgs(Set<Edge> edgs) {
		this.edgs = edgs;
	}

	public void setNodes(Set<Node> nodes) {
		this.nodes = nodes;
	}

}
