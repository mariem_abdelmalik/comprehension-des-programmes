package parsage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.PackageDeclaration;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import spoon.Launcher;
import spoon.MavenLauncher;
import spoon.reflect.CtModel;
import spoon.reflect.declaration.CtType;


public class Parser {

	private String projectSource;
	private CtModel model;

	public Parser(String projectSource) throws IOException {

		this.setProjectSource(projectSource);
		this.model = this.generatModel();

	}

	public CtModel generatModel() {

		Launcher launcher = new Launcher();
		launcher.addInputResource(projectSource);
		launcher.buildModel();

		CtModel model = launcher.getModel();

		return model;
	}
	
	public Set<String> getAllClasses() {
		
		Set<String> classes = new HashSet<String>();

		for(CtType<?> s : this.model.getAllTypes()) {
			classes.add(s.getQualifiedName());
			}

		return classes;
	}

	public String getProjectSource() {
		return projectSource;
	}

	public void setProjectSource(String projectSource) {
		this.projectSource = projectSource;
	}

	public CtModel getModel() {
		return model;
	}

	public void setModel(CtModel model) {
		this.model = model;
	}




	

}
