package visialisation;

import com.mxgraph.layout.mxCircleLayout;
import com.mxgraph.model.mxGraphModel;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxUtils;

import grapheCouplage.Edge;
import grapheCouplage.GrapheCouplageWeighted;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.ext.JGraphXAdapter;
import org.jgrapht.graph.AbstractBaseGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleGraph;

import javax.swing.*;
import java.awt.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class UndirectedWeightedGraph extends JFrame {

	Set<Edge> edgs = new HashSet<Edge>();

	public static class MyEdge extends DefaultWeightedEdge {
		@Override
		public String toString() {
			return String.valueOf(getWeight());
		}
	}

	public UndirectedWeightedGraph(Set<Edge> edgs) {

		this.edgs = edgs;

		JGraphXAdapter<String, MyEdge> jgxAdapter;

		UndirectedGraph<String, MyEdge> g = new SimpleGraph<String, MyEdge>(MyEdge.class);

		for (Edge edge : this.edgs) {

			g.addVertex(edge.getNodeA().getName());
			g.addVertex(edge.getNodeB().getName());

			MyEdge e = g.addEdge(edge.getNodeA().getName(), edge.getNodeB().getName());

			((AbstractBaseGraph<String, MyEdge>) g).setEdgeWeight(e, edge.getWeighte());

		}

		jgxAdapter = new JGraphXAdapter<String, MyEdge>(g);
		mxGraphComponent graphComponent = new mxGraphComponent(jgxAdapter);
		mxGraphModel graphModel = (mxGraphModel) graphComponent.getGraph().getModel();
		Collection<Object> cells = graphModel.getCells().values();
		// This part to remove arrow from edge
		mxUtils.setCellStyles(graphComponent.getGraph().getModel(), cells.toArray(), mxConstants.STYLE_ENDARROW,
				mxConstants.NONE);
		getContentPane().add(graphComponent);

		mxCircleLayout layout = new mxCircleLayout(jgxAdapter);
		layout.execute(jgxAdapter.getDefaultParent());

		this.setTitle(" Graph de couplage ");
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.setPreferredSize(new Dimension(400, 400));
		this.pack();
		this.setVisible(true);

	}

	public Set<Edge> getEdgs() {
		return edgs;
	}

	public void setEdgs(Set<Edge> edgs) {
		this.edgs = edgs;
	}

}