package visialisation;

import java.io.IOException;
import java.util.Set;

import grapheCouplage.Edge;
import parsage.Parser;
import grapheCouplage.GrapheCouplageWeighted;

public class visialisation {

	public static void main(String[] args) throws IOException {

		final String projectPath = "/home/mariem/Bureau/test";
		// final String projectSourcePath = projectPath + "/src";
		
		final String jrePath = "/home/mariem/.p2/pool/plugins/org.eclipse.justj.openjdk.hotspot.jre.full.linux.x86_64_18.0.2.v20220815-1350/jre";

		
		Parser parser = new Parser(projectPath, jrePath);

		GrapheCouplageWeighted grapheCouplageWeighted = new GrapheCouplageWeighted();

		grapheCouplageWeighted.initGrapheCouplageWeighted(parser.getCompilationUnits());

		new UndirectedWeightedGraph(grapheCouplageWeighted.getEdgs());

	}
}
