package parsage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.PackageDeclaration;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import visitor.MethodDeclarationVisitor;
import visitor.TypeDeclarationVisitor;

public class Parser {
	
	private String projectSource;
	private String jrePath;
	private ArrayList<File> javaFiles = new ArrayList<File>();
	private ArrayList<CompilationUnit> compilationUnits = new ArrayList<CompilationUnit>();


	public Parser (String projectSource,String jrePath) throws IOException{
		
		this.setJrePath(jrePath);
		this.setProjectSource(projectSource);
		String projectSourcePath = projectSource + "/src";
		final File folder = new File(projectSourcePath);
		initJavaFiles(folder);
		this.compilationUnits= this.parse(this.javaFiles);
		
	}
	
		
	private ArrayList<CompilationUnit> parse(ArrayList<File> javaFiles) throws IOException {
		
		String projectSourcePath = this.projectSource + "/src";
		
		String[] sources = { projectSourcePath }; 
		
		String[] classpath = {jrePath};
		
		ASTParser parser = ASTParser.newParser(AST.JLS4); // java +1.6
		
		parser.setResolveBindings(true);
		
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		
		parser.setBindingsRecovery(true);
		
		Map options = JavaCore.getOptions();
		
		parser.setCompilerOptions(options);
		
		parser.setUnitName("");

		for (File fileEntry : javaFiles) {
			
				String content = FileUtils.readFileToString(fileEntry);
				
				parser.setEnvironment(classpath,sources, new String[] { "UTF-8"}, true);
				
				parser.setSource(content.toCharArray());
								
				CompilationUnit compilationUnit = (CompilationUnit) parser.createAST(null); 
				
				compilationUnits.add(compilationUnit);
				
				}
		
		return compilationUnits;
		
	}
	
	public void initJavaFiles(File folder){
		
		for (File file : folder.listFiles()) {
			
			if (file.isFile() && file.getName().contains(".java")) {
				this.javaFiles.add(file);
			}
			
			else {
				initJavaFiles(file);
			}
		}
	}
	
	

	public ArrayList<CompilationUnit> getCompilationUnits() {
		return compilationUnits;
	}

	public void setCompilationUnits(ArrayList<CompilationUnit> compilationUnits) {
		this.compilationUnits = compilationUnits;
	}

	public String getProjectSource() {
		return projectSource;
	}

	public void setProjectSource(String projectSource) {
		this.projectSource = projectSource;
	}

	public String getJrePath() {
		return jrePath;
	}

	public void setJrePath(String jrePath) {
		this.jrePath = jrePath;
	}


	public ArrayList<File> getJavaFiles() {
		return javaFiles;
	}


	public void setJavaFiles(ArrayList<File> javaFiles) {
		this.javaFiles = javaFiles;
	}
	
	
	
	
}
