package grapheCouplage;

import java.util.ArrayList;
import java.util.List;

import parsage.Parser;
import visitor.MethodDeclarationVisitor;
import visitor.MethodInvocationVisitor;
import visitor.TypeDeclarationVisitor;

import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.TypeDeclaration;

public class Couplage {

	private List<CompilationUnit> compilationUnits;

	public Couplage(List<CompilationUnit> compilationUnits) {

		super();
		this.compilationUnits = compilationUnits;
		initTotalAppel();

	}

	private int totalAppel;

	public int nbAppelMethod(TypeDeclaration A, TypeDeclaration B) {

		int appelAB = 0;

		MethodDeclarationVisitor visitor = new MethodDeclarationVisitor();
		A.accept(visitor);

		MethodDeclarationVisitor visitor2 = new MethodDeclarationVisitor();
		B.accept(visitor2);

		for (MethodDeclaration methodA : visitor.getMethods()) {

			MethodInvocationVisitor visitor3 = new MethodInvocationVisitor();
			methodA.accept(visitor3);

			for (MethodInvocation methodInvocation : visitor3.getMethods()) {

				for (MethodDeclaration methodB : visitor2.getMethods()) {
					
					System.out.println(methodB.getName().toString()+" "+methodInvocation.getName().toString());

					if (methodB.getName().toString().equals(methodInvocation.getName().toString())) {
						appelAB = appelAB + 1;
					}
				}

			}
		}

		return appelAB;

	}

	public void initTotalAppel() {

		List<TypeDeclaration> typeDeclarations = new ArrayList<TypeDeclaration>();

		for (CompilationUnit compilationUnit : this.compilationUnits) {

			TypeDeclarationVisitor visitor = new TypeDeclarationVisitor();

			compilationUnit.accept(visitor);

			typeDeclarations.addAll(visitor.getTypes());
		}
		for (int i = 0; i < typeDeclarations.size() - 1; i++) {

			for (int j = i + 1; j < typeDeclarations.size(); j++) {

				this.totalAppel = this.totalAppel
						+ this.nbAppelMethod(typeDeclarations.get(i), typeDeclarations.get(j));

				this.totalAppel = this.totalAppel
						+ this.nbAppelMethod(typeDeclarations.get(j), typeDeclarations.get(i));

			}
		}
	}

	public Double couplage(TypeDeclaration A, TypeDeclaration B) {

		Double couplageAB = (double) 0;

		Double nbAppelAB = (double) this.nbAppelMethod(A, B);

		Double nbAppelBA = (double) this.nbAppelMethod(B, A);

		couplageAB = (nbAppelAB + nbAppelBA) / this.totalAppel;

		System.out.println("nbAppelAB : "+nbAppelAB+" nbAppelBA : "+nbAppelBA+"A : "+A.getName().toString()+" B : "+B.getName());
		
		System.out.println();
		
		return couplageAB;

	}

	public List<CompilationUnit> getCompilationUnits() {
		return compilationUnits;
	}

	public void setCompilationUnits(List<CompilationUnit> compilationUnits) {
		this.compilationUnits = compilationUnits;
	}

	public int getTotalAppel() {
		return totalAppel;
	}

	public void setTotalAppel(int totalAppel) {
		this.totalAppel = totalAppel;
	}

}
