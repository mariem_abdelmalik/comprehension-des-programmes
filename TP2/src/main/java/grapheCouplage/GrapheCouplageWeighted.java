package grapheCouplage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.TypeDeclaration;

import visitor.MethodDeclarationVisitor;
import visitor.MethodInvocationVisitor;
import visitor.TypeDeclarationVisitor;

public class GrapheCouplageWeighted {

	Set<Node> nodes = new HashSet<Node>();
	Set<Edge> edgs = new HashSet<Edge>();

	public void initGrapheCouplageWeighted(List<CompilationUnit> compilationUnits) {

		Couplage couplage = new Couplage(compilationUnits);
		
		couplage.setCompilationUnits(compilationUnits);

		List<TypeDeclaration> typeDeclarations = new ArrayList<TypeDeclaration>();
		
		for (CompilationUnit compilationUnit : compilationUnits) {

			TypeDeclarationVisitor visitor = new TypeDeclarationVisitor();

			compilationUnit.accept(visitor);

			typeDeclarations.addAll(visitor.getTypes());

		}
		
		System.out.println(typeDeclarations.size());
		
		for (int i = 0; i < typeDeclarations.size() - 1; i++) {

			for (int j = i + 1; j < typeDeclarations.size(); j++) {

				System.out.println(couplage.couplage(typeDeclarations.get(i), typeDeclarations.get(j)));
				if (couplage.couplage(typeDeclarations.get(i), typeDeclarations.get(j)) > 0) {

					Node nodeA = new Node(typeDeclarations.get(i).getName().toString());
					Node nodeB = new Node(typeDeclarations.get(j).getName().toString());

					nodes.add(nodeA);
					nodes.add(nodeB);

					Edge edge = new Edge(nodeA, nodeB,
							couplage.couplage(typeDeclarations.get(i), typeDeclarations.get(j)));

					edgs.add(edge);

				}

			}
		}
	}

	// verification de l'existance d'edge dans la liste des edges.
	public int edgeAlreadyExists(Edge edge, List<Edge> edges) {

		for (int i = 0; i < edges.size(); i++) {

			if (edge.getNodeA().getName().equals(edges.get(i).getNodeA().getName())
					&& edge.getNodeB().equals(edges.get(i).getNodeB().getName())) {

				return i;

			}
		}

		return -1;
	}

	public ArrayList<Node> getNodes() {
		return (ArrayList<Node>) nodes;
	}

	public Set<Edge> getEdgs() {
		return edgs;
	}

	public void setEdgs(Set<Edge> edgs) {
		this.edgs = edgs;
	}

	public void setNodes(Set<Node> nodes) {
		this.nodes = nodes;
	}

}
