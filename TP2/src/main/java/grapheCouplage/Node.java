package grapheCouplage;

import org.eclipse.jdt.core.dom.SimpleName;

public class Node{
	String name;

	public Node(String name){
		this.name=name;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Override
	public boolean equals(Object object){

		Node node = (Node) object;
		if(this.getName()==node.getName()){
			return true;
	    } 
		return false;
	    
	}
	
}
